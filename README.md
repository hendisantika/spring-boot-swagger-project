# Spring Boot Swagger Project

## To do list things:
1. Clone the repository: `git clone https://gitlab.com/hendisantika/spring-boot-swagger-project.git`. 
2. Go to your folder: `cd spring-boot-swagger-project`
3. Run the application: `mvn clean spring-boot:run`
4. Open your favorite browser: http://localhost:8080/swagger-ui.html 
 
## Screen shot

### Swagger UI
![Swagger UI](img/swagger.png "Swagger UI")