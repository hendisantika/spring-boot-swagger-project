package com.hendisantika.springbootswaggerproject;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootSwaggerProjectApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringBootSwaggerProjectApplication.class, args);
    }

}
