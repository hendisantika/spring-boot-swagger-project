package com.hendisantika.springbootswaggerproject.repository;

import com.hendisantika.springbootswaggerproject.domain.Book;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-swagger-project
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 21/12/19
 * Time: 15.59
 */
@Repository
public interface BookRepository extends PagingAndSortingRepository<Book, Long> {
}